;(function(){
  
  var React = require('react');
  var ReactDOM = require('react-dom');
  var Canvas = require('./canvas');
  var Sketchpad = require('./sketchpad');
  
  ReactDOM.render(React.createElement(Sketchpad), document.querySelector('.page-wrapper'));

})();