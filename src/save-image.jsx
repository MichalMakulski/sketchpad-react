var React = require('react');

module.exports = React.createClass({
  render: function() { 
    return <div className="save-image">
        <input 
          type="text"
          placeholder="Drawing title"
          value={this.props.drawingTitle} 
          onChange={this.handleInput} 
        />
        <button onClick={this.props.saveImage} type="button">Save drawing</button>
      </div>
  },
  handleInput: function(ev) {
    var text = ev.target.value;
    this.props.updateDrawingTitle(text);
  }
});