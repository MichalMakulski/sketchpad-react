var ReactDOM = require('react-dom');
var React = require('react');

module.exports = React.createClass({
  componentDidMount() {
    this.canvas = ReactDOM.findDOMNode(this);
    this.ctx = this.canvas.getContext('2d');
    this.points = [];
  },
  onMouseDown: function(ev) {
		this.points = [];	
    this.props.updateCanvasState();
		this.points.push({ x: ev.clientX, y: ev.clientY });
  },
  onMouseUp: function() {
    var image = this.canvas.toDataURL();
    this.props.updateCanvasState(image);
		this.points.length = 0;
  },
  onMouseMove: function(ev) {
		if (!this.props.isDrawing) {
      return;
    } 
			this.points.push({ x: ev.clientX, y: ev.clientY });
			this.ctx.beginPath();
			this.ctx.moveTo(this.points[this.points.length - 2].x, this.points[this.points.length - 2].y);
			this.ctx.lineTo(this.points[this.points.length - 1].x, this.points[this.points.length - 1].y);
			this.ctx.stroke();
		for (var i = 0, len = this.points.length; i < len; i++) {
				dx = this.points[i].x - this.points[this.points.length-1].x;
				dy = this.points[i].y - this.points[this.points.length-1].y;
				d = dx * dx + dy * dy;
			if (d < 1000) {
				this.ctx.beginPath();
				this.ctx.strokeStyle = 'rgba(0,0,0,0.3)';
				this.ctx.moveTo( this.points[this.points.length-1].x + (dx * 0.2), this.points[this.points.length-1].y + (dy * 0.2));
				this.ctx.lineTo( this.points[i].x - (dx * 0.2), this.points[i].y - (dy * 0.2));
				this.ctx.stroke();
			}
		}
	},
  render: function() {
    return <canvas 
      onMouseDown={this.onMouseDown}
      onMouseUp={this.onMouseUp}
      onMouseMove={this.onMouseMove}
      width={window.innerWidth}
      height={window.innerHeight}
      >
    </canvas>
  }
});