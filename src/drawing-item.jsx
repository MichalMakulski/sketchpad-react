var React = require('react');

module.exports = React.createClass({
  
  render: function() {
    return <li className="drawing">
      <h2> {this.props.title} </h2>
      <img src={this.props.image} />
      <span onClick={this.props.deleteDrawing.bind(null, this.props.uid)} className="delete-drawing-btn">DELETE</span>
    </li>
  } 
  
});