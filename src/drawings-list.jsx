var React = require('react');
var DrawingItem = require('./drawing-item');

module.exports = React.createClass({
  render: function() {
    var content = this.getDrawings();
    if(!content.length) {
      return <div className="drawer">
      <h2>Nothing's here</h2>
    </div>
    }else {
      var drawings = content.map(this.processDrawing);
      return <div className="drawer">
        <ul>
          {drawings}
        </ul>
      </div>
    }
    
  },
  getDrawings: function() {
    var children = [];

    for(var key in this.props.drawings) {
      var drawing = this.props.drawings[key];
      drawing.uid = key;
      children.push(drawing);
    }
    return children;
  },
  processDrawing: function(drawing, idx) {
    return <DrawingItem 
      deleteDrawing={this.props.deleteDrawing}
      key={drawing.uid} 
      uid={drawing.uid} 
      title={drawing.title} 
      image={drawing.image} 
    />
  }
  
});