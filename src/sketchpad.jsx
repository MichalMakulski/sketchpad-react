var React = require('react');
var ReactFire = require('reactfire');
var Firebase = require('firebase');
var Canvas = require('./canvas');
var DrawingsList = require('./drawings-list');
var SaveImage = require('./save-image');
var canvasUtils = require('./canvas-utils');
var dbUrl = 'https://sketchpad.firebaseio.com/';

module.exports = React.createClass({
  mixins: [ ReactFire, canvasUtils ],
  getInitialState: function() {
    return {
      isDrawing: false,
      imageTitle: '',
      imageData: undefined,
      drawerOpen: false,
      drawings: {}
    }
  },
  componentWillMount: function() {
    this.fb = new Firebase(dbUrl + 'drawings/');
    this.bindAsObject(this.fb, 'drawings');
    this.fb.on('value', this.handleDataLoaded);
  },
  handleDataLoaded: function(){
    this.setState({loaded: true});
  },
  render: function() {
    return <div className={"sketchpad-container" + (this.state.drawerOpen ? ' drawer-open' : '') }>
      <Canvas 
        isDrawing={this.state.isDrawing}
        updateCanvasState={this.updateCanvasState}
      />
      <DrawingsList 
        drawings={this.state.drawings} 
        deleteDrawing={this.deleteDrawing}
      />
      <span onClick={this.toggleDrawer} className="toggle-drawer-btn"></span>
      <SaveImage 
        updateDrawingTitle={this.updateDrawingTitle}
        saveImage={this.saveImage}
        drawingName={this.state.imageTitle}
      />
    </div>
  }
});