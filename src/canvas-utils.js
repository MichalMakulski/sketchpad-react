module.exports = {
  updateCanvasState: function(image) {
    this.setState({
      isDrawing: !this.state.isDrawing,
      imageData: image
    });
  },
  saveImage: function() {
    this.firebaseRefs.drawings.push({
      title: this.state.imageTitle,
      image: this.state.imageData
    }).then(function(){
      alert('Drawing saved');
    });
  },
  updateDrawingTitle: function(value) {
    this.setState({
      imageTitle: value
    });
  },
  toggleDrawer: function() {
    this.setState({
      drawerOpen: !this.state.drawerOpen
    });
  },
  deleteDrawing: function(drawingID) {
    this.fb.child(drawingID).remove();
  }
}